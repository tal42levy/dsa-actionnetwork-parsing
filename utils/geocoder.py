import requests
import geojson
import csv

from turfpy.measurement import boolean_point_in_polygon
from geojson import Point, MultiPolygon, Polygon

#from keys import keys 

HERE_URL = "https://geocoder.ls.hereapi.com/6.2/geocode.json"
CENSUS_URL = "https://geocoding.geo.census.gov/geocoder/locations/addressbatch"

def geocodeRow(row):
    if row['billing_address_line_1'] == '':
        return 0, 0
    search_text = ""
    search_text += f"{row['billing_address_line_1']} "
    search_text += f"{row['billing_city']}, "
    search_text += f"{row['billing_state']} "
    search_text += f"{row['billing_zip']}"

    params = { 'searchtext': search_text, 'apiKey': HERE_KEY}

    res = requests.get(HERE_URL, params=params)
    json = res.json()
    
    lat, lng = json['Response']['View'][0]['Result'][0]['Location']['DisplayPosition']['Latitude'], json['Response']['View'][0]['Result'][0]['Location']['DisplayPosition']['Longitude']
    return lat, lng

def geocode(members):
    neighborhoods = decodeGeojson('neighborhood_IRTC.geojson')
    districts = decodeGeojson('116th_congressional_districts.geojson')
    for i, mem in enumerate(members):
        mem["latitude"], mem["longitude"] = geocodeRow(mem)
        mem["neighborhood_unit"] = findMemberNeighborhood(mem, neighborhoods)
        mem["congressional_district"] = findMemberCD(mem, districts)
        print(f"{i+1} members geolocated")

def decodeGeojson(filename):
    with open(filename) as file:
        geojson_file = geojson.load(file)
        features = []
        for feature in geojson_file['features']:
            if feature['geometry']['type'] == 'GeometryCollection':
                geometry = MultiPolygon(
                    feature['geometry']['geometries'])
            elif feature['geometry']['type'] == 'MultiPolygon':
                geometry = MultiPolygon(feature['geometry'])
            else:
                geometry = Polygon(feature['geometry'])
            if filename == 'neighborhood_IRTC.geojson':
                geometry['area'] = feature['properties']['name']
            elif filename == '116th_congressional_districts.geojson':
                geometry['area'] = feature['properties']['district']
            features.append(geometry)
        return features

def findMemberNeighborhood(member, neighborhoods):
    point = Point([member['longitude'], member['latitude']])
    for polygon in neighborhoods:
        if boolean_point_in_polygon(point, polygon):
            return polygon['area']

def findMemberCD(member, districts):
    point = Point([member['longitude'], member['latitude']])
    for polygon in districts:
        if boolean_point_in_polygon(point, polygon):
            print(polygon)
            return polygon

def geocodeCensus(members):
    with open('temp.csv', 'w', newline='') as temp_file:
        keys = ['ak_id', 'address_line_1', 'city', 'state', 'zip']
        csv_writer = csv.DictWriter(temp_file, keys)
        pruned_members = [{x: member[x] for x in keys} for member in members ]
        csv_writer.writerows(pruned_members)

    neighborhoods = decodeGeojson('neighborhood_IRTC.geojson')
    with open('temp.csv', 'r', newline='') as temp_file:
        files = {'addressFile': ('temp.csv', temp_file), 'benchmark': (None, 9)}
        r = requests.post(CENSUS_URL, files=files)
        location_string_list = r.text.strip().split("\n")
        location_list = [location_string.split('","') for location_string in location_string_list]
        response_keys = ['ak_id', 'billing_address_line_1', 'billing_city', 'billing_state', 'billing_zip', 'match', 'exact', 'address_full', 'coordinates', 'something', 'r']
        for member in members:
            coordinates_string = [location[5] for location in location_list if location[0][1:] == member['ak_id']][0]
            coordinates = coordinates_string.split(",")
            member['longitude'] = coordinates[0]
            member['latitude'] = coordinates[1]

