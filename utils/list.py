# Utils for managing lists of members, especially the lists we get from National
import datetime
from collections import defaultdict
import utils.parsing as parsing

merge_map = {}  # TODO: set it up so this isn't store in code


def get_expired(all_members, date=None):
    if date is None:
        today = datetime.datetime.now()
        date = [today.year, today.month, today.day]

    expired = [m for m in all_members if m["xdate_parsed"] <= date]
    migs = [m for m in all_members if m["xdate_parsed"] > date]
    return expired, migs


def split_families(members):
    family_accounts = [m for m in members if m["family_first_name"]]
    to_add = []
    for f in family_accounts:
        first_name = f["family_first_name"].strip("& ").replace(",\n", "")
        last_name = f["family_last_name"].strip("& ").replace(",\n", "")
        ak_id = f["ak_id"] + "f"

        new_val = f.copy()
        new_val["first_name"] = first_name
        new_val["last_name"] = last_name
        new_val["ak_id"] = ak_id
        new_val["email"] = ""
        to_add.append(new_val)
    return to_add + members


def merge_dupes(all_members):
    email_map = defaultdict(lambda: [])
    for m in all_members:
        email_map[m["email"]].append(m)
    for email1, email2 in merge_map.items():
        rows1 = email_map.get(email1)
        rows2 = email_map.get(email2)

        if rows1 and rows2:
            join_date = min(rows1 + rows2, key=lambda x: x["join_date_parsed"])
            x_date = min(rows1 + rows2, key=lambda x: x["xdate_parsed"])

            for val in rows1:
                val["join_date"] = join_date["join_date"]
                val["xdate"] = x_date["join_date"]
            for val in rows2:
                del email_map[email2]
    return sum(email_map.values(), [])


def add_branch(rows, branch_map):
    error = False
    for r in rows:
        zipcode = r["billing_zip"][:5]
        r["zip5"] = zipcode
        branch = branch_map.get(zipcode)
        if branch is None:
            print("Couldn't get branch for {}".format(zipcode))
            error = True
        r["branch"] = branch

    if error:
        raise ValueError("Some members with zip codes not in the branch list")
    return rows


def get_newly_expired(new_members, old_list, date=None, lookback=2, last_date=None):  # months
    print("Check for newly Expired")
    if date is None:
        today = datetime.datetime.now()
        date = [today.year, today.month, today.day]
    if last_date is None:
        last_date = [date[0], date[1] - lookback, date[2]]
    last_date_array = last_date.split("-")
    last_date_array[0] = int(last_date.split("-")[0])
    last_date_array[1] = int(last_date.split("-")[1])
    last_date_array[2] = int(last_date.split("-")[2])
    print(date, last_date_array)

    date_one_month = [date[0], date[1] + 1, date[2]]
    if date_one_month[1] == 13:
        date_one_month = [date[0] + 1, 1, date[2]]

    new = []
    new_expired = []
    new_and_expired = []
    soon_to_expire = []
    missing = []
    renewed = []
    migs_safe = []
    expired = []

    l1 = []
    l2 = []
    l3 = []
    l4 = []

    old_list = list(old_list)
    old_map = {str(o["ak_id"]): o for o in old_list}
    found_map = {str(o["ak_id"]): False for o in old_list}

    for val in new_members:
        if val["ak_id"] in old_map:
            #print("Match")
            found_map[val["ak_id"]] = True
            xdate = val["xdate_parsed"]
            old_vals = old_map[val["ak_id"]]

            prev_expired = old_vals["xdate_parsed"] <= last_date_array
            now_expired = xdate <= date
            if now_expired and prev_expired:
                l1.append(val)
            elif not now_expired and prev_expired:
                l2.append(val)
            elif now_expired and not prev_expired:
                l3.append(val)
            else:
                l4.append(val)

            if xdate > last_date_array and xdate <= date:
                new_expired.append(val)
            elif xdate > last_date_array and xdate <= date_one_month:
                soon_to_expire.append(val)
            elif xdate <= date:
                expired.append(val)
            else:
                migs_safe.append(val)

            if old_vals["xdate_parsed"] < xdate and old_vals["xdate_parsed"] <= date:
                renewed.append(val)
        else:
            if val["xdate_parsed"] <= date:
                new_and_expired.append(val)
            else:
                new.append(val)

    print(len(l1), len(l2), len(l3), len(l4), len(new), len(new_and_expired))
    print(
        len(new),
        len(new_and_expired),
        len(migs_safe) + len(soon_to_expire),
        len(new_expired) + len(expired),
    )

    old = [f for f in old_list if f["xdate_parsed"] > last_date_array]
    for ak_id, found in found_map.items():
        if not found:
            missing.append(old_map[ak_id])

    return new_expired, new, missing, [], renewed, soon_to_expire, new_and_expired
