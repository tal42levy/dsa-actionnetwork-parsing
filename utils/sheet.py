# Utils file for interactions with the Google Sheets API
import os
import json
import time
import utils.parsing as parsing

from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

FULL_RANGE = "$A$1:$YY"
FILE = os.path.join(os.path.dirname(__file__), "..", "service_creds.json")
SCOPES = [
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/spreadsheets",
]

def connect():
    credentials = service_account.Credentials.from_service_account_file(FILE, scopes=SCOPES)
    service = build("sheets", "v4", credentials=credentials, cache_discovery=False)
    return service.spreadsheets()


def get_service_email():
    with open(FILE, "r") as f:
        return json.loads(f.read())["client_email"]


def get_sheets(api, spreadsheet_id):
    try:
        return api.get(spreadsheetId=spreadsheet_id).execute().get("sheets")
    except HttpError:
        msg = "Error accessing sheet! Try sharing it with {}".format(get_service_email())
        raise ValueError(msg)


def get_sheet_data(api, spreadsheet_id, sheet_id=None, range=FULL_RANGE):
    if sheet_id is None:
        sheet = get_sheets(api, spreadsheet_id)[0]
        sheet_id = sheet.get("properties", {}).get("title", "Sheet1")
    rangetopull = "{}!{}".format(sheet_id, range)
    result = api.values().get(spreadsheetId=spreadsheet_id, range=rangetopull).execute()
    rows = result.get("values", [])
    return rows

def read_sheet_list(api, sheet_id, range=FULL_RANGE, sheet_name=None):
    rows = get_sheet_data(api, sheet_id, sheet_id=sheet_name, range=range)
    title_row = rows[0]
    headers = [s.lower() for s in title_row]
    objects = ({t: v for t, v in zip(headers, row)} for row in rows[1:])
    return objects, headers

def read_member_list(api, sheet_id, range=FULL_RANGE, sheet_name=None):
    objects, headers = read_sheet_list(api, sheet_id, range, sheet_name)
    #--\/-- ONLY NEEDED WHEN CSV DO NOT MATCH --\/--#
    #objects = convert_headers(objects, headers, header_new)
    objects = map(parsing.clean_record, objects)
    return list(objects), headers

def convert_headers(objects, headers, header_new):
    new_objects = []
    for o in objects:
        new_objc = {
        "ak_id": o["ak_id"],
        "first_name": o["first_name"],
        "middle_name": o["middle_name"],
        "last_name": o["last_name"],
        "suffix": o["suffix"],
        "billing_address_line_1": o["address_line_1"],
        "billing_address_line_2": o["address_line_2"],
        "billing_city": o["city"],
        "billing_state": o["state"],
        "billing_zip": o["zip"],
        "mailing_address1": "",
        "mailing_address2": "",
        "mailing_city": "",
        "mailing_state": "",
        "mailing_zip": "",
        "mobile_phone": o["mobile_phone"],
        "home_phone": o["home_phone"],
        "work_phone": o["work_phone"],
        "email": o["email"],
        "mail_preference": o["mail_preference"],
        "do_not_call": o["do_not_call"],
        "join_date": o["join_date"],
        "xdate": o["xdate"],
        "membership_type": o["membership_type"],
        "monthly_dues_status": o["monthly_status"],
        "membership_status": o["membership_status"],
        "union_member": o["union_member"],
        "union_name": o["union_name"],
        "union_local": o["union_local"],
        "student_yes_no": o["student_yes_no"],
        "student_school_name": o["student_school_name"],
        "ydsa chapter": o["ydsa chapter"],
        "dsa_chapter": o["chapter"]
        }
        new_objects.append(new_objc)
    return new_objects
        

def add_sheet(api, title, spreadsheet_id, data, headers=None):
    body = {"requests": [{"addSheet": {"properties": {"title": title}}}]}
    print("Adding Batch")
    try:
        api.batchUpdate(spreadsheetId=spreadsheet_id, body=body).execute()
    except HttpError as e:
        if e.resp.get("content-type","").startswith("application/json"):
            content = json.loads(e.content)
            reason = content.get("error").get("message")
            if "already exists. Please enter another name" in reason:
                pass
            else:
                print(content)
                raise
        else:
            print("Could not get error form contnet-type")
            raise

    range = "{}!1:{}".format(title, len(data) + 1)
    writebody = {"majorDimension": "ROWS", "values": parsing.to_lists(data, headers)}
    while True:
        try:
            api.values().update(
                spreadsheetId=spreadsheet_id, range=range, valueInputOption="RAW", body=writebody,
            ).execute()
            return
        except HttpError as e:
            print("Errored {}, retrying in 5 seconds...".format(e))
            time.sleep(5)
