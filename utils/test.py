import utils.airtable as airtable
import utils.actionnetwork as an
import utils.parsing as parsing
import utils.list as list_utils
import utils.sheet as sheet_utils

person = {
    "ak_id": "163340",
    "dsa_id": "",
    "first_name": "Natasha",
    "middle_name": "",
    "last_name": "Aasadore",
    "suffix": "",
    "organization": "",
    "address_line_1": "12223 Spring Trl",
    "address_line_2": "",
    "city": "Kagel Canyon",
    "state": "CA",
    "z ip": "91342-5816",
    "country": "United States",
    "mobile_phone": "8183885202",
    "home_phone": "",
    "work_phone": "",
    "email": "natashaforhomes@gmail.com",
    "mail_preference": "Yes",
    "do_not_call": "FALSE",
    "join_date": "2020-04-08",
    "xdate": " 2021-04-08",
    "memb_status": "M",
    "membership_type": "annual",
    "monthly_status": "never",
    "chapter": "Los Angeles",
    "membership_status": "member in good standing",
    "union_member": "No",
    "xdate_parsed": [2021, 4, 8],
    "join_date_parsed": [2020, 4, 8],
    "xdate_save": "2021-04-08",
    "xdate_actionnetwork": "2021/4/8",
    "join_date_actionnetwork": "2020/4/8",
    "branch": "SFV",
}

branch = "Eastside"
zips = airtable.get_table("Neighborhood Zips", branch)
print(zips)
zip_map = {z["fields"]["Zip"]: z["id"] for z in zips}
print(zip_map)
# print(airtable.member_exists(person, members))
# row = airtable.transform_contact_row(test_person, branch)
# print(airtable.put_row('All Mobilizees', branch, [row]).json())
