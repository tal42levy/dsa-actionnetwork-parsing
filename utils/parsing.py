# Utils file for parsing rows of member data

import re
import csv
import time
import datetime
import requests
import utils.actionnetwork as an
from geopy.geocoders import Nominatim, GoogleV3

CODER = None
LACITY_URL = "https://api.lacity.org/boe_geoquery/addressvalidationservice?address={}&status=new&layerset=neighborhoodinfo&apikey=B5thBZ3BXlR1waoUvderfnrMETLwk2SE"


def lookup_lacity_info(address):
    mem_url = LACITY_URL.format(address)
    print(address)
    resp = requests.get(mem_url).json()
    if "layers" in resp:
        return resp["layers"]["neighborhood council"]["name"]
    elif "multipleMatch" in resp:
        for r in resp["locations"]:
            if r["zipcode"] == address["zip5"]:
                return lookup_lacity_info(r["addressName"])
    else:
        return None


def lookup_lacity_info_list(members):
    for i, mem in enumerate(members):
        if "los angeles" in mem["billing_city"].lower():
            print(i)
            addr = mem["billing_address_line_1"] + ", " + mem["zip5"]
            nc = lookup_lacity_info(addr)
            if nc is None and "#" in addr:
                nc = lookup_lacity_info(addr.split("#")[0])
            if nc is None:
                print(addr, "failed to lookup")
            mem["neighborhood_council"] = nc
            an.post_signup(mem)
        else:
            mem["neighborhood_council"] = None


def load_csv(filename):
    with open(filename, "r") as f:
        headers = [s.lower() for s in f.read().split("\n")[0].split(",")]
    with open(filename, "r") as f:
        reader = csv.DictReader(f)
        vals = list(reader)[1:]
        return vals, headers


def to_lists(vals, headers=[]):
    if not headers:
        headers = [v for v in vals[0].keys() if "_parsed" not in v]
    return [headers] + [[v.get(h) for h in headers] for v in vals]


def write_csv(filename, vals, headers=[]):
    if not vals:
        with open(filename, "w") as f:
            f.write("")
        return
    if not headers:
        headers = list(vals[0].keys())
    with open(filename, "w") as f:
        header_vals = ",".join(headers)
        f.write(header_vals + "\n")
        joined_headers = [",".join([r.get(k, "") or "" for k in headers]) for r in vals]
        vals = "\n".join(joined_headers)
        f.write(vals)


# PITA, but needed for gmail
def dedupe_emails(email):
    if not email:
        return None
    email = email.lower().strip(", .")
    return email


def strip_phone(phone):
    if phone is None:
        return phone
    return phone.replace(" ", "").replace("-", "").replace("(", "").replace(")", "")


def parse_number(vals, index):
    dashed = re.match("(\d{3})-(\d{3})-(\d{4})", vals[index])
    dashed_once = re.match("(\d{3})-(\d{7})", vals[index])
    dotted = re.match("(\d{3})\.(\d{3})\.(\d{4})", vals[index])

    if re.match("\d{9}", vals[index]):
        return vals[index]
    if re.match("\+\d{9}", vals[index]):
        return vals[index][1:]
    elif dashed_once:
        return dashed_once.group(1) + dashed_once.group(2)
    elif dashed:
        return dashed.group(1) + dashed.group(2) + dashed.group(3)
    elif dotted:
        return dotted.group(1) + dotted.group(2) + dotted.group(3)
    elif re.match("\d{3}", vals[index]) and len(vals[index]) == 3:
        second_half = parse_second_half(index, vals)
        if second_half:
            return vals[index] + second_half
    elif re.match("\(\d{3}\)", vals[index]) and len(vals[index]) == 5:
        second_half = parse_second_half(index, vals)
        if second_half:
            return vals[index][1:-1] + second_half
    return None


def parse_second_half(index, vals):
    if len(vals) == index:
        return None
    if not vals[index]:
        return parse_second_half(index + 1, vals)

    dashed = re.match("(\d{3})-(\d{4})", vals[index + 1])
    if dashed:
        return dashed.group(1) + dashed.group(2)
    elif re.match("\d{7}", vals[index + 1]):
        return vals[index + 1]
    elif re.match("\d{3}", vals[index + 1]) and len(vals[index + 1]) == 3:
        if re.match("\d{4}", vals[index + 2]):
            return vals[index + 1] + vals[index + 2]


def fix_phone_numbers(row):
    phone_keys = ["mobile_phone", "home_phone", "work_phone"]
    all_phones = []
    for key in phone_keys:
        vals = row[key].split(" ")
        for i, phone_number in enumerate(vals):
            fixed_phone = parse_number(vals, i)
            if fixed_phone is not None:
                all_phones.append(fixed_phone)

    all_phones = list(set(all_phones))
    for i, phone_key in enumerate(phone_keys):
        if i < len(all_phones):
            row[phone_key] = all_phones[i]
        else:
            row[phone_key] = ""


def get_google(address):
    url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address={}&key=AIzaSyBOhfoXirERnzNxIN_Kq19Teo1iNHSDcTI"
    url = url.format(address)
    ret = requests.get(url).json()
    if len(ret["results"]) == 0:
        print("no results", address, ret)
        return None, None
    ret = ret["results"][0]["geometry"]
    return ret["location"]["lat"], ret["location"]["lng"]


def clean_address(address):
    for val in ["#", "Ste", "Apt", "Suite", "apt", "Unit", "unit", "APT", "Rm"]:
        if val in address:
            return address.split(val)[0]
    if "/" in address:
        idx = address.find("/")
        return address[:idx]

    return address


def get_lat_long(row):
    global CODER
    if CODER is None:
        CODER = Nominatim(user_agent="sorry_address_finder")
    address1 = clean_address(row["billing_address_line_1"])
    full_address = "{} {} {}".format(address1, row["billing_city"], row["billing_zip"])
    try:
        location = CODER.geocode(full_address, timeout=10)
    except Exception as e:
        print(e)
        return None, None

    lat, lng = 100, 100
    if location is not None:
        lat, lng = location.latitude, location.longitude

    if lat > 35 or lat < 32 or lng > -115 or lng < -120:
        full_address = "{} {} {} {}".format(address1, row["billing_city"], row["billing_state"], row["billing_zip"])
        lat, lng = get_google(full_address)

    return lat, lng


def geocode(rows):
    for r in rows:
        lat, lng = get_lat_long(r)
        r["latitude"] = lat
        r["longitude"] = lng
        time.sleep(1)


def pad_year(year):
    if year < 30:
        year += 2000
    elif year < 100:
        year += 1900
    return year


def same_date(old_date, date):
    if not isinstance(old_date, str):
        return False

    splitted = old_date.split("/")
    if len(splitted) != 3:
        return False
    if len(splitted[-1]) > 2:
        splitted[-1] = splitted[-1][:-2]

    splitted2 = old_date.split("/")
    if len(splitted2) != 3:
        return False
    if len(splitted2[-1]) > 2:
        splitted2[-1] = splitted2[-1][:-2]

    return splitted2 == splitted


def parse_date(date_str):
    # Make sure it's always year-month-day
    str_splitted = date_str.replace("/", "-").split("-")
    if len(str_splitted[2]) > 4:
        str_splitted[2] = str_splitted[2].split(" ")[0]

    date_splitted = [int(f) for f in str_splitted]
    if date_splitted[0] > 12 and date_splitted[1] <= 12:
        date_splitted[0] = pad_year(date_splitted[0])
        return date_splitted
    elif date_splitted[1] <= 12 and date_splitted[2] > 12:
        return [pad_year(date_splitted[2]), date_splitted[0], date_splitted[1]]
    elif date_splitted[1] > 12:  # must be month-day-year
        return [pad_year(date_splitted[2]), date_splitted[0], date_splitted[1]]
    elif str_splitted[2][0] == "0":  # must be month-day-year
        return [pad_year(date_splitted[2]), date_splitted[0], date_splitted[1]]
    elif len(str_splitted[0]) == 1:  # must be month-day-year
        return [pad_year(date_splitted[2]), date_splitted[0], date_splitted[1]]
    else:  # just assume it's month-day-year, it's impossible to tell
        return [pad_year(date_splitted[2]), date_splitted[0], date_splitted[1]]


def is_before(date1, date2):
    if int(date1[0]) != int(date2[0]):
        return int(date1[0]) < int(date2[0])
    if int(date1[1]) != int(date2[1]):
        return int(date1[1]) < int(date2[1])
    if int(date1[2]) != int(date2[2]):
        return int(date1[2]) < int(date2[2])


def clean_record(row):
    now = datetime.datetime.utcnow()
    for val in ["xdate", "join_date", "x_date"]:
        if val in row:
            row["{}_parsed".format(val)] = parse_date(row[val])
    for val in ["email", "email1", "email2", "email3", "email4"]:
        if val in row:
            row[val] = dedupe_emails(row[val])

    for k, v in row.items():
        if isinstance(v, str) and v.lower() == "true":
            row[k] = True
        elif isinstance(v, str) and v.lower() == "false":
            row[k] = False
        if isinstance(v, str):
            row[k] = v.replace(",", "").replace("\n", "")

    # Cmon national wtf
    # If monthly is active, member has one year before dues fully expire
    # If failed, delete regular xdate so that previous xdate stays
    if row["monthly_dues_status"] == "active":
        parsed = [now.year + 1, now.month, now.day]
        row["xdate_parsed"] = parsed
        row["xdate_save"] = "{}/{}/{}".format(parsed[1], parsed[2], parsed[0] - 2000)
        row["xdate_actionnetwork"] = "{}/{}/{}".format(parsed[0], parsed[1], parsed[2])
    else:
        row["xdate_save"] = row["xdate"]
        row["xdate_actionnetwork"] = "{}/{}/{}".format(
            row["xdate_parsed"][0], row["xdate_parsed"][1], row["xdate_parsed"][2]
        )
    row["join_date_actionnetwork"] = "{}/{}/{}".format(
        row["join_date_parsed"][0], row["join_date_parsed"][1], row["join_date_parsed"][2],
    )
    fix_phone_numbers(row)
    return row
