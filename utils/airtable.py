import os
import json
import requests

from collections import defaultdict
from keys import AIRTABLE_KEY, BASES
from utils.parsing import strip_phone

BASE_API_URL = "https://api.airtable.com/v0/"

TABLE_CACHE = defaultdict(dict)


def get_table(table_name, base_name=None):
    if TABLE_CACHE[base_name].get(table_name) is not None:
        return TABLE_CACHE[base_name][table_name]
    url = os.path.join(BASE_API_URL, BASES[base_name], table_name)
    params = {"api_key": AIRTABLE_KEY}
    all_records = []

    res = requests.get(url, params=params).json()
    if "records" not in res:
        print(res)
    all_records += res["records"]
    while res.get("offset"):
        params["offset"] = res["offset"]
        res = requests.get(url, params=params).json()
        all_records += res["records"]
    TABLE_CACHE[base_name][table_name] = all_records
    return all_records


def put_row(table_name, base_name, records):
    url = os.path.join(BASE_API_URL, BASES[base_name], table_name)
    headers = {
        "Authorization": "Bearer {}".format(AIRTABLE_KEY),
        "Content-Type": "application/json",
    }
    body = {"records": [{"fields": r} for r in records], "typecast": True}
    return requests.post(url, data=json.dumps(body), headers=headers)


def lookup_zip(zip_code, base_name, table_name="Neighborhood Zips"):
    all_zips = get_table(table_name, base_name)
    zip_map = {z["fields"]["Zip"]: z["id"] for z in all_zips}
    return zip_map.get(zip_code)


def transform_contact_row(row, branch):
    zip_key = lookup_zip(row["billing_zip"][:5], branch)
    if zip_key is None:
        print("MISSING ZIP", branch, row["billing_zip"][:5])
        return
    return {
        "Contact Name": "{} {}".format(row["first_name"], row["last_name"]),
        "Contact Email": row["email"],
        "Contact Phone": row["mobile_phone"],
        "Contact Zip": zip_key,
        "Is contact a member?": True,
        "New Member?": True,
    }


def member_exists(member, branch_list):
    for l in branch_list:
        row = l["fields"]
        phone = strip_phone(row.get("Contact Phone"))
        if phone == member["mobile_phone"]:
            return True
        elif row.get("Contact Email") == member["email"] and not member["mobile_phone"]:
            return True
    return False
