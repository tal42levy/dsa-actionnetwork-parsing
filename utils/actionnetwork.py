import json
import requests
from keys import AN_KEY

BASE_API_URL = "https://actionnetwork.org/api/v2"
ALL_TAGS = {}


def get(url, is_full_url=False):
    headers = {"Content-Type": "application/json", "OSDI-API-Token": AN_KEY}
    if is_full_url:
        full_url = url
    else:
        full_url = "{}/{}".format(BASE_API_URL, url)
    val = requests.get(full_url, headers=headers)
    if val.status_code == 404:
        print("ERROR!", full_url, val.text)
    return val


def post(url, body):
    headers = {"Content-Type": "application/json", "OSDI-API-Token": AN_KEY}
    full_url = "{}/{}".format(BASE_API_URL, url)
    val = requests.post(full_url, data=json.dumps(body), headers=headers)
    if val.status_code == 404:
        print("ERROR!", full_url, val.text)
    return val


def put(url, body, is_full=False):
    headers = {"Content-Type": "application/json", "OSDI-API-Token": AN_KEY}
    if not is_full:
        full_url = "{}/{}".format(BASE_API_URL, url)
    else:
        full_url = url
    return requests.put(full_url, data=json.dumps(body), headers=headers)


def hello_world():
    print(get("").text)


def get_minimal_person(person_dict, tags=[]):
    email = {"address": person_dict["email"]}
    field_map = {
        "Dues Expiration Date": person_dict["xdate_actionnetwork"],
        "Join_Date": person_dict["join_date_actionnetwork"],
        "Action Kit ID": person_dict["ak_id"],
        "Branch": person_dict["branch"],
        "Neighborhood Council": person_dict["neighborhood_council"],
    }
    obj = {"person": {"email_addresses": [email], "custom_fields": field_map}}
    if tags:
        obj["add_tags"] = tags
    return obj


def update_person(person_dict, an_person):
    address = [person_dict["billing_address_line_1"]]
    person = get_minimal_person(person_dict)

    matching_address = next(a for a in an_person["postal_addresses"] if a["primary"])
    put = False
    if not matching_address.get("region"):
        address = [
            {
                "postal_code": person_dict["billing_zip"],
                "address_lines": address,
                "locality": person_dict["billing_city"],
                "region": person_dict["billing_state"]
                #"country": person_dict["country"],
            }
        ]
        person["person"]["postal_addresses"] = address
        put = True

    if "Phone" not in an_person["custom_fields"] and person_dict["mobile_phone"]:
        person["person"]["custom_fields"]["Phone"] = person_dict["mobile_phone"]
        put = True

    if an_person.get("family_name") is None and person_dict["last_name"]:
        person["person"]["family_name"] = person_dict["last_name"]
        put = True
    if an_person.get("given_name") is None and person_dict["first_name"]:
        person["person"]["given_name"] = person_dict["first_name"]
        put = True

    return person, put


def get_tag_by_id(tag_id):
    if tag_id not in ALL_TAGS:
        tag = get("tags/{}".format(tag_id)).json()["name"]
        ALL_TAGS[tag_id] = tag
    return ALL_TAGS[tag_id]


def get_tags(an_person):
    taggings = get(an_person["_links"]["osdi:taggings"]["href"], True).json()
    tags = []
    for t in taggings["_embedded"]["osdi:taggings"]:
        tag_id = t["_links"]["osdi:tag"]["href"].split("/")[-1]
        tags.append(get_tag_by_id(tag_id))
    return tags


def post_signup(person_dict, tags=[]):
    dict_to_post = get_minimal_person(person_dict, tags)
    an_person = post("people/", dict_to_post).json()
    # an_person["tags"] = get_tags(an_person)
    updated_put, should_put = update_person(person_dict, an_person)

    if should_put:
        return post("people/", updated_put).json()
    else:
        return an_person
