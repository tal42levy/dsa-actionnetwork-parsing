from collections import defaultdict
from googleapiclient.errors import HttpError

import time
import utils.parsing as parsing
import utils.list as list_utils
import utils.actionnetwork as an
import utils.airtable as airtable
import utils.sheet as sheet_utils
import utils.geocoder as geocoder

SHEETID = "1RxIxSG9W6n3-hrHbcxU0MKm0dnZFnkZqBYRborWsXOk"
BRANCH_SHEETID = "1-7b-PFkoonQ3HEhgQaAcuqoZiXs3KXRn2toCXpznEg0"
BRANCHES = ["Central", "Westside", "SFV", "Eastside/SGV", "South Central / Inglewood"]
BRANCH_LIST_IDS = {
    "Central": "1o3dpDJ0pmdNiX3O9-DFv1KYZHN3wNNKexOqjtr9Y8BI",
    "Westside": "12D3uHjw8iQ0HKvY3SwqziT_z8iI20ZgK66jehTOpfwI",
    "SFV": "1KR3cgLZ0OIxDKE3_-muj3QgiaxBZxhELrwN9I88-pyA",
    "Eastside/SGV": "1Q-zx9vvnRdUyDrmRFAF24TlPusLzdnrmJAtrwAYO4ns",
    "South Central / Inglewood": "16hHewn1jozld7tiSqY1C0KstnhWbIqBstIWtp47GGOA",
}

LABOR_SHEETID = "1LzWvCKdvYCH9DalueuUEOArmKLWhex0TrnPnyaK2W_E"
YDSA_SHEETID = "1aM1f8z7tt3VSHMDErj2tKEFWHx5-AkIkUaLwanjHZlI"
REMOVING_VALS = [
    "billing_address_line_1",
    "billing_address_line_2",
    "billing_zip",
    "xdate_actionnetwork",
    "join_date_actionnetwork",
    "xdate_actionnetwork",
    "xdate_save",
    "memb_status",
    "monthly_dues_status",
    "dsa_chapter",
    "mail_preference" "organization",
    "dsa_id",
]


def split(sheet_id):
    # Parses national lists
    api = sheet_utils.connect()
    # check if already parsed

    # if so, just count people in each sheet and return vals
    # sheets = sheet_utils.get_sheets(api, sheet_id)
    try:
        rows, headers = sheet_utils.read_member_list(api, sheet_id)
    except HttpError:
        return "Error accessing sheet! Try sharing it with {}".format(
            sheet_utils.get_service_email()
        )

    all_members = list_utils.merge_dupes(list(rows))
    families = [f for f in all_members if f["family_first_name"]]
    nonfamilies = [f for f in all_members if not f["family_first_name"]]
    sheet_utils.add_sheet(api, "Individual", sheet_id, nonfamilies, headers)
    sheet_utils.add_sheet(api, "Family", sheet_id, families, headers)
    return "DONE"


def geocode(sheet_id):
    api = sheet_utils.connect()
    sheets = sheet_utils.get_sheets(api, sheet_id)
    titles = {s.get("properties", {}).get("title"): s for s in sheets}
    if "All Records" not in titles:
        return "Parse this sheet first"
    try:
        rows, headers = sheet_utils.read_member_list(api, sheet_id, sheet_name="All Records")
    except HttpError:
        return "Error accessing sheet! Try sharing it with {}".format(
            sheet_utils.get_service_email()
        )

    count = 0
    rows = list(rows)
    for r in reversed(rows):
        if not r.get("latitude") or not r.get("longitude"):
            r["latitude"], r["longitude"] = geocoder.geocodeRow([r])
            count += 1
            if count % 10 == 0:
                print(count, "rows complete")
    sheet_utils.add_sheet(api, "All Records", sheet_id, rows, headers)
    return "Success"


def parse_route(sheet_id):
    # check if already parsed
    reparse = request.args.get("reparse")
    date = request.args["date"]
    geocode = request.args.get("geocode")
    old_sheet_id = request.args.get("old_sheet_id")
    last_date = request.args.get("last_date")

    return parse(sheet_id, date, reparse, geocode, old_sheet_id, last_date)


def post_migs_to_an(migs):
    for i, m in enumerate(migs):
        if i % 100 == 0:
            print(i, "migs to an")
        an.post_signup(m)


def add_new_tag(new):
    for m in new:
        an.post_signup(m, tags=["brand_new_migs"])
    print("added {} new migs to AN".format(len(new)))


def post_new_to_at(new):
    contact_table = "All Contacts"
    branch_maps = {key: airtable.get_table(contact_table, key) for key in BRANCHES}

    count = defaultdict(int)
    for m in new:
        branch = m["branch"]
        if not airtable.member_exists(m, branch_maps[branch]):
            count[branch] += 1
            row = [airtable.transform_contact_row(m, branch)]
            time.sleep(0.1)
            airtable.put_row(contact_table, branch, row)
    print(count, "new mobilizer contacts")


def read_rows_from_sheet(sheet_id, api):
    # if so, just count people in each sheet and return vals
    sheets = sheet_utils.get_sheets(api, sheet_id)
    titles = {s.get("properties", {}).get("title"): s for s in sheets}
    rows, headers = sheet_utils.read_member_list(api, sheet_id)
    return rows, headers, sheets


def parse_rows(rows, old_list, date, last_date, sheet_id, reparse=False):
    print("Parse_Rows")
    #for o in old_list:
    #    print(o)
    #    break
    api = sheet_utils.connect()
    branch_sheet, _ = sheet_utils.read_sheet_list(api, BRANCH_SHEETID)
    branch_map = {b["5zip"]: b["branch"] for b in branch_sheet}

    members = collect_data(rows, old_list, date, last_date, branch_map)
    if not reparse:
        post_new_to_at(members["new"])
        post_migs_to_an(members["migs"])
        add_new_tag(members["new"])

    all_members = members["all_members"]
    headers = [h for h in all_members[0].keys() if "_parsed" not in h]

    sheet_utils.add_sheet(api, "Last Records", sheet_id, old_list, headers)
    sheet_utils.add_sheet(api, "All Records", sheet_id, members["all_members"], headers)
    sheet_utils.add_sheet(api, "MIGS", sheet_id, members["migs"], headers)
    sheet_utils.add_sheet(api, "Expired", sheet_id, members["expired"], headers)
    sheet_utils.add_sheet(api, "Monthly Failed", sheet_id, members["monthly_failed"], headers)
    sheet_utils.add_sheet(api, "New", sheet_id, members["new"], headers)
    sheet_utils.add_sheet(api, "New Expired", sheet_id, members["new_expired"], headers)
    sheet_utils.add_sheet(api, "Missing", sheet_id, members["missing"], headers)
    sheet_utils.add_sheet(api, "Renewed", sheet_id, members["renewed"], headers)
    sheet_utils.add_sheet(api, "Expiring Soon", sheet_id, members["soon_to_expire"], headers)

    post_labor_sheet(api, members, headers)
    post_ydsa_sheet(api, members, headers)
    post_branch_sheets(api, members, headers)

    return ", ".join(
        [
            "{} Total Records".format(len(all_members)),
            "{} MIGS".format(len(members["migs"])),
            "{} New Expired".format(len(members["new_expired"])),
            "{} New".format(len(members["new"])),
            "{} Monthly Failed".format(len(members["monthly_failed"])),
            "{} Expired".format(len(members["expired"])),
            "{} Missing".format(len(members["missing"])),
            "{} Renewed".format(len(members["renewed"])),
            "{} Expiring Soon".format(len(members["soon_to_expire"])),
        ]
    )


def parse(sheet_id, date, reparse, geocode, old_sheet_id, last_date):
    api = sheet_utils.connect()
    # Parses national lists
    raw_rows, headers, sheets = read_rows_from_sheet(sheet_id, api)
    headers.append("zip5")

    titles = {s.get("properties", {}).get("title"): s for s in sheets}
    if "MIGS" in titles and not reparse:
        # results
        titles_to_report = [
            "MIGS",
            "Expired",
            "New",
            "New Expired",
            "Missing",
            "Renewed",
            "Expiring Soon",
            "Monthly Failed",
        ]
        report = ""
        for s in sheets:
            title = s["properties"].get("title")
            if title in titles_to_report:
                data = sheet_utils.get_sheet_data(api, sheet_id, title)
                report += "{} {}, ".format(len(data) - 1, title)
        return report

    try:
        date = parsing.parse_date(date)
    except KeyError:
        date = None

    try:
        last_date = parsing.parse_date(last_date)
    except:
        last_date = None

    if old_sheet_id:
        old_list, _ = sheet_utils.read_member_list(api, old_sheet_id, sheet_name="All Records")
    else:
        old_list = []

    branch_sheet, _ = sheet_utils.read_sheet_list(api, BRANCH_SHEETID)
    branch_map = {b["5zip"]: b["branch"] for b in branch_sheet}

    members = collect_data(raw_rows, old_list, date, last_date, branch_map)
    if not reparse:
        post_migs_to_an(members["migs"])
        add_new_tag(members["new"])
        post_new_to_at(members["new"])

    all_members = members["all_members"]
    headers = [h for h in all_members[0].keys() if "_parsed" not in h]

    if geocode:
        if 'latitude' not in headers:
            headers += ["latitude"]
        if 'longitude' not in headers:
            headers += ["longitude"]
        if 'neighborhood_unit' not in headers:
            headers += ["neighborhood_unit"]
        geocoder.geocode(members["all_members"])

    sheet_utils.add_sheet(api, "All Records", sheet_id, members["all_members"], headers)
    sheet_utils.add_sheet(api, "MIGS", sheet_id, members["migs"], headers)
    sheet_utils.add_sheet(api, "Expired", sheet_id, members["expired"], headers)
    sheet_utils.add_sheet(api, "Monthly Failed", sheet_id, members["monthly_failed"], headers)
    sheet_utils.add_sheet(api, "New", sheet_id, members["new"], headers)
    sheet_utils.add_sheet(api, "New Expired", sheet_id, members["new_expired"], headers)
    sheet_utils.add_sheet(api, "Missing", sheet_id, members["missing"], headers)
    sheet_utils.add_sheet(api, "Renewed", sheet_id, members["renewed"], headers)
    sheet_utils.add_sheet(api, "Expiring Soon", sheet_id, members["soon_to_expire"], headers)

    post_labor_sheet(api, members, headers)
    post_ydsa_sheet(api, members, headers)
    post_branch_sheets(api, members, headers)

    return ", ".join(
        [
            "{} Total Records".format(len(all_members)),
            "{} MIGS".format(len(members["migs"])),
            "{} New Expired".format(len(members["new_expired"])),
            "{} New".format(len(members["new"])),
            "{} Monthly Failed".format(len(members["monthly_failed"])),
            "{} Expired".format(len(members["expired"])),
            "{} Missing".format(len(members["missing"])),
            "{} Renewed".format(len(members["renewed"])),
            "{} Expiring Soon".format(len(members["soon_to_expire"])),
        ]
    )


def post_branch_sheets(api, members, headers):
    headers_short = headers[:]
    for val in REMOVING_VALS:
        if val in headers_short and "address" not in val:
            headers_short.remove(val)
    for branch_name, branch_id in BRANCH_LIST_IDS.items():
        branch_mems = [m for m in members["migs"] if m["branch"] == branch_name]
        branch_new_mems = [m for m in members["new"] if m["branch"] == branch_name]
        branch_expired = [m for m in members["expired"] if m["branch"] == branch_name]
        sheet_utils.add_sheet(api, "Members", branch_id, branch_mems, headers_short)
        sheet_utils.add_sheet(api, "New Members", branch_id, branch_new_mems, headers_short)
        sheet_utils.add_sheet(api, "Expired", branch_id, branch_expired, headers_short)


def post_ydsa_sheet(api, members, headers):
    headers_short = headers[:]
    for val in REMOVING_VALS:
        if val in headers_short:
            headers_short.remove(val)
    ydsa_mems = [m for m in members["migs"] if m.get("student_school_name")]
    new_ydsa_mems = [m for m in members["new"] if m.get("student_school_name")]
    sheet_utils.add_sheet(api, "Members", YDSA_SHEETID, ydsa_mems, headers_short)
    sheet_utils.add_sheet(api, "New Members", YDSA_SHEETID, new_ydsa_mems, headers_short)


def post_labor_sheet(api, members, headers):
    headers_short = headers[:]
    for val in REMOVING_VALS:
        if val in headers_short:
            headers_short.remove(val)
    union_mems = [m for m in members["migs"] if m.get("union_name_local")]
    new_union_mems = [m for m in members["new"] if m.get("union_name_local")]
    sheet_utils.add_sheet(api, "Members", LABOR_SHEETID, union_mems, headers_short)
    sheet_utils.add_sheet(api, "New Members", LABOR_SHEETID, new_union_mems, headers_short)


def collect_data(all_members, old_list, date, last_date, branch_map):
    print("Collect_data!")
    #for o in old_list:
    #    print(o)
    #    break
    mems = {}
    all_members = list_utils.add_branch(list_utils.merge_dupes(list(all_members)), branch_map)
    parsing.lookup_lacity_info_list(all_members)
    mems["all_members"] = all_members
    mems["expired"], mems["migs"] = list_utils.get_expired(all_members, date)

    mems["monthly_failed"] = [f for f in all_members if f["monthly_dues_status"] == "2mo_plus_failed"]
    (
        mems["new_expired"],
        mems["new"],
        mems["missing"],
        mems["changes"],
        mems["renewed"],
        mems["soon_to_expire"],
        mems["new_and_expired"],
    ) = list_utils.get_newly_expired(all_members, old_list, date, last_date=last_date)
    return mems


def signin():
    target_email = request.args.get("email")
    if not target_email:
        return "You need to pass an email!"

    sheet = sheet_utils.connect()
    rows = sheet_utils.get_sheet_data(sheet, SHEETID)

    title_row = rows[0]
    email_idx = title_row.index("email")
    xdate_idx = title_row.index("xdate")

    found_record = False
    for record in rows[1:]:
        if record[email_idx] == target_email:
            found_record = record

    if found_record:
        date = found_record[xdate_idx]
        message = "You're on our list! Your membership expires {}".format(date)
    else:
        message = "You aren't on our list!"

    return message
