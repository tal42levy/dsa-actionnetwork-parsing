from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from parser import parse, parse_rows
import utils.parsing as parsing
import utils.sheet as sheet_utils

    ####----------------------------------------####
    #### ----- START OF ARGS PROCESSING ----- ####
    ####----------------------------------------####
if __name__ == "__main__":
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sheet_id", type=str, help="New sheet of members")
    parser.add_argument("old_sheet_id", type=str, help="Old sheet of members")
    parser.add_argument(
        "--file",
        "-f",
        dest="file",
        default=False,
        help="Set true if using a local file",
        action="store_true",
    )
    parser.add_argument(
        "--reparse",
        "-r",
        dest="reparse",
        default=False,
        help="Set true if reparsing an old list",
        action="store_true",
    )
    parser.add_argument(
        "--geocode",
        "-g",
        dest="geocode",
        default=False,
        help="Set true if you want to geocode addresses",
        action="store_true",
    )
    parser.add_argument("--date", type=str, help="date of new sheet")
    parser.add_argument("--last_date", type=str, help="date of old sheet")
    args = parser.parse_args()
    ####----------------------------------------####
    #### ----- END OF ARGS PROCESSING ----- ####
    ####----------------------------------------####
    try:
        with open('CSV-Log.txt') as f:
            for line in f:
                pass
            last_line = line
            print(last_line)
            last_date_file, temp = last_line.split(" | ")
            last_date = last_date_file
    except:
        print("Something went wrong getting last date form CSV-Log.txt, either it does not exist or parsing was off.")
        print("Will attempt to use args.last_date if it was used.")
        last_date = parsing.parse_date(args.last_date)
    

    if args.file:
        rows, _ = parsing.load_csv(args.sheet_id)
        rows = [parsing.clean_record({k.lower(): v for k, v in row.items()}) for row in rows]
        date = parsing.parse_date(args.date)
        api = sheet_utils.connect()
        if args.reparse:
            old_list, old_header = sheet_utils.read_member_list(api, args.old_sheet_id, sheet_name="Last Records")
        else:
            old_list, old_header = sheet_utils.read_member_list(api, args.old_sheet_id, sheet_name="All Records")
        print("Running on "+str(len(rows))+" rows, Running against "+str(len(old_list))+" rows")
        val = parse_rows(rows, list(old_list), date, last_date, args.old_sheet_id, args.reparse)
    else:
        val = parse(args.sheet_id, args.date, True, args.geocode, args.old_sheet_id, args.last_date)
    print(val)
    f = open("CSV-Log.txt", "a")
    f.write(str(args.date) + " | " + val)
    f.close()
