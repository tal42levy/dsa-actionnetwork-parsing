Readme For DSA Member Parser

<h1>Setup

Install python3 
Linux: sudo apt install python3
Install pipenv (not need if on linux)
pipenv shell (Initate shell)
pip3 install -r requirements.txt

Files with keys that are needed.
actionnetwork_key.py
keys.py
service_creds.json
These keys will be provided by one of the admins

# Usage
$ pipenv shell
$ python3 run.py
